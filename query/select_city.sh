#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="select_city"
echo "drop table $OUTPUT_TABLE"
$MYSQL -e "drop table $OUTPUT_TABLE"|| true

echo "create table $OUTPUT_TABLE as ( remote select code as value, name as name from ( select code, name from bigobject.citymapping order by code asc ) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"
$MYSQL -e "create table $OUTPUT_TABLE as ( remote select code as value, name as name from ( select code, name from bigobject.citymapping order by code asc ) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"
