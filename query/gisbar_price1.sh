#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}

type_TIMES=\"${TIMES//,/\",\"}\"

CITY=${CITY// /,}
type_CITY=\"${CITY//,/\",\"}\"

AREA=${AREA// /,}
type_AREA=\"${AREA//,/\",\"}\"
#if [[ $TIMES != '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#elif [[ $TIMES == '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" and type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" and type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#fi


#以下送進BO做查詢
aa=${id##*--}
aa=\"${aa//,/\",\"}\"

if [[ $id == '' ]] ;then
	#echo "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	if [[ $AREA == '' ]] ;then
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) "
	elif [[ $AREA != '' ]] ;then
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
	fi
elif [[ $id != '' ]] && [[ $AREA == '' ]];then
	#echo "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
	where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($aa)"

elif [[ $id != '' ]] && [[ $AREA != '' ]];then
	aaa=${id##*--}
	AREA2=${AREA}
	if [[ $AREA2 != *"$aaa"* ]] ;then
		#echo "33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333"
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
	else
		#echo "55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555"
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($aa) "
	fi
fi

echo "create table $OUTPUT_TABLE as 
	( 	select villages, fee_type, round(fee * 3.3058/10000,2) as fee, y.villages as villages2, y.cnt as fee2, '交易量' as fee_type2 from
		(select *, case when fee_type = 'avg_price' then '1' else '0' end as join from
			(remote select villages, avg(unit_price) as avg_price, min(unit_price) as min_price, max(unit_price) as max_price from agent.lvr_main_a a
				left join bigobject.citymapping b on a.type1 = b.code
				left join agent.lvr_main_a_detail c on a.id_number = c.id_number
				where unit_price <> 0  $where_state 
				group by villages order by villages asc 
				partition by db='bigobject' and alias = 'TWSBOCLIENT1')
			UNPIVOT (villages, fee FOR fee_type IN(*)))x
		left join 
		(remote select villages, '1' as join, count(distinct id_number) as cnt from agent.lvr_main_a a
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where unit_price <> 0  $where_state 
			group by villages order by villages asc 
			partition by db='bigobject' and alias = 'TWSBOCLIENT1')y on x.villages = y.villages and x.join = y.join
	)"

$MYSQL -e "create table $OUTPUT_TABLE as 
	( 	select villages, fee_type, round(fee * 3.3058/10000,2) as fee, y.villages as villages2, y.cnt as fee2, '交易量' as fee_type2 from
		(select *, case when fee_type = 'avg_price' then '1' else '0' end as join from
			(remote select villages, avg(unit_price) as avg_price, min(unit_price) as min_price, max(unit_price) as max_price from agent.lvr_main_a a
				left join bigobject.citymapping b on a.type1 = b.code
				left join agent.lvr_main_a_detail c on a.id_number = c.id_number
				where unit_price <> 0  $where_state 
				group by villages order by villages asc 
				partition by db='bigobject' and alias = 'TWSBOCLIENT1')
			UNPIVOT (villages, fee FOR fee_type IN(*)))x
		left join 
		(remote select villages, '1' as join, count(distinct id_number) as cnt from agent.lvr_main_a a
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where unit_price <> 0  $where_state 
			group by villages order by villages asc 
			partition by db='bigobject' and alias = 'TWSBOCLIENT1')y on x.villages = y.villages and x.join = y.join
	)"

#echo "create table $OUTPUT_TABLE as 
#	( 	select villages, fee_type, round(fee * 3.3058/10000,2) as fee, y.villages as villages2, y.cnt as fee2, '交易量' as fee_type2 from
#		(select *, case when fee_type = 'avg_price' then '1' else '0' end as join from
#			(remote select villages, avg(unit_price) as avg_price, min(unit_price) as min_price, max(unit_price) as max_price from agent.lvr_main_a a
#				left join bigobject.citymapping b on a.type1 = b.code
#				left join agent.lvr_main_a_detail c on a.id_number = c.id_number
#				where not address like '%地號%' and unit_price <> 0  $where_state 
#				group by villages order by villages asc 
#				partition by db='bigobject' and alias = 'TWSBOCLIENT1')
#			UNPIVOT (villages, fee FOR fee_type IN(*)))x
#		left join 
#		(remote select villages, '1' as join, count(distinct id_number) as cnt from agent.lvr_main_a a
#			left join bigobject.citymapping b on a.type1 = b.code
#			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
#			where not address like '%地號%' and unit_price <> 0  $where_state 
#			group by villages order by villages asc 
#			partition by db='bigobject' and alias = 'TWSBOCLIENT1')y on x.villages = y.villages and x.join = y.join
#	)"
#
#$MYSQL -e "create table $OUTPUT_TABLE as 
#	( 	select villages, fee_type, round(fee * 3.3058/10000,2) as fee, y.villages as villages2, y.cnt as fee2, '交易量' as fee_type2 from
#		(select *, case when fee_type = 'avg_price' then '1' else '0' end as join from
#			(remote select villages, avg(unit_price) as avg_price, min(unit_price) as min_price, max(unit_price) as max_price from agent.lvr_main_a a
#				left join bigobject.citymapping b on a.type1 = b.code
#				left join agent.lvr_main_a_detail c on a.id_number = c.id_number
#				where not address like '%地號%' and unit_price <> 0  $where_state 
#				group by villages order by villages asc 
#				partition by db='bigobject' and alias = 'TWSBOCLIENT1')
#			UNPIVOT (villages, fee FOR fee_type IN(*)))x
#		left join 
#		(remote select villages, '1' as join, count(distinct id_number) as cnt from agent.lvr_main_a a
#			left join bigobject.citymapping b on a.type1 = b.code
#			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
#			where not address like '%地號%' and unit_price <> 0  $where_state 
#			group by villages order by villages asc 
#			partition by db='bigobject' and alias = 'TWSBOCLIENT1')y on x.villages = y.villages and x.join = y.join
#	)"






