#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="select_town"
echo "drop table $OUTPUT_TABLE"
$MYSQL -e "drop table $OUTPUT_TABLE"|| true



CITY=${CITY// /,}
type1=\"${CITY//,/\",\"}\"


echo "create table $OUTPUT_TABLE as ( remote select villages as value, villages as name from (select villages from agent.lvr_main_a where villages <> '' and type1 in ($type1) group by villages order by villages desc) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"
$MYSQL -e "create table $OUTPUT_TABLE as ( remote select villages as value, villages as name from (select villages from agent.lvr_main_a where villages <> '' and type1 in ($type1) group by villages order by villages desc) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"
