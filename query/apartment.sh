#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}
######## 取得前一季的時間節點
TIMES3=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) where value < '$TIMES'  order by value desc limit 1 "`

type_TIMES=\'${TIMES//,/\',\'}\'
type_TIMES3=\'${TIMES3//,/\',\'}\'
where_time_new=$type_TIMES','$type_TIMES3

CITY=${CITY// /,}
type_CITY=\'${CITY//,/\',\'}\'

AREA=${AREA// /,}
type_AREA=\'${AREA//,/\',\'}\'

#以下送進BO做查詢
aa=${id##*--}
aa=\"${aa//,/\",\"}\"


if [[ $id == '' ]] ;then
	#echo "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	if [[ $AREA == '' ]] ;then
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) "
	elif [[ $AREA != '' ]] ;then
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($type_AREA) "
	fi
elif [[ $id != '' ]] && [[ $AREA == '' ]];then
	#echo "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
	where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($aa)"

elif [[ $id != '' ]] && [[ $AREA != '' ]];then
	aaa=${id##*--}
	AREA2=${AREA}
	if [[ $AREA2 != *"$aaa"* ]] ;then
		#echo "33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333"
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($type_AREA) "
	else
		#echo "55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555"
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($aa) "
	fi
fi

read -ra text_var <<< `$MYSQL -se "select round(unit_price * 3.3058) as unit_price from (remote select time, avg(unit_price) as unit_price from ( select time, unit_price, type1, id_number, address, building_type from agent.lvr_main_a  $where_state  ) a left join bigobject.citymapping b on a.type1 = b.code left join agent.lvr_main_a_detail c on a.id_number = c.id_number where not address like '%地號%' and building_type like '%公寓%'  group by time order by time desc partition by db='bigobject' and alias = 'TWSBOCLIENT1')"`
var1=${text_var[0]}
var2=${text_var[1]}
if [ -Z ${var1} ];then
	var1="0.00"
fi
if [ -Z ${var2} ];then
	var1="0.00"
fi


if [ $var1 -gt $var2 ]; then
    # 紅色上漲
    echo "create table $OUTPUT_TABLE as 
        (select concat('<strong><font size=\"5\">公寓&nbsp&nbsp</font><font size=\"5\">','</font><font size=\"3\"></font></strong>','<i class=\"fa fa-sort-asc fa-2x\" aria-hidden=\"true\" style=\"color:red\"></i>') as TITLE, concat('均單價',fee,'萬') as VALUE from ( select case when fee = 0.00 then '--' else cast(round(fee) as INT32) end as fee from (select cast(round($var1*1.00/10000,4) as DOUBLE) as fee) ) )"
    $MYSQL -e "create table $OUTPUT_TABLE as 
        (select concat('<strong><font size=\"5\">公寓&nbsp&nbsp</font><font size=\"5\">','</font><font size=\"3\"></font></strong>','<i class=\"fa fa-sort-asc fa-2x\" aria-hidden=\"true\" style=\"color:red\"></i>') as TITLE, concat('均單價',fee,'萬') as VALUE from ( select case when fee = 0.00 then '--' else cast(round(fee) as INT32) end as fee from (select cast(round($var1*1.00/10000,4) as DOUBLE) as fee) ) )"
else
    # 綠色下跌
    echo "create table $OUTPUT_TABLE as 
    	(select concat('<strong><font size=\"5\">公寓&nbsp&nbsp</font><font size=\"5\">','</font><font size=\"3\"></font></strong>','<i class=\"fa fa-sort-desc fa-2x\" aria-hidden=\"true\" style=\"color:green\"></i>') as TITLE, concat('均單價',fee,'萬') as VALUE from ( select case when fee = 0.00 then '--' else cast(round(fee) as INT32) end as fee from (select cast(round($var1*1.00/10000,4) as DOUBLE) as fee) ) )"
    $MYSQL -e "create table $OUTPUT_TABLE as 
    	(select concat('<strong><font size=\"5\">公寓&nbsp&nbsp</font><font size=\"5\">','</font><font size=\"3\"></font></strong>','<i class=\"fa fa-sort-desc fa-2x\" aria-hidden=\"true\" style=\"color:green\"></i>') as TITLE, concat('均單價',fee,'萬') as VALUE from ( select case when fee = 0.00 then '--' else cast(round(fee) as INT32) end as fee from (select cast(round($var1*1.00/10000,4) as DOUBLE) as fee) ) )"
fi
