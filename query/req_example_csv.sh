#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin
#---------------------- settings ----------------------#
BOIP=${BO%%:*}
MYSQL="mysql -h ${BOIP} -P 3306"

columns=${COLUMN// /,}


arr=()
if [ ! -z "$channel_name_filter" ]
then
	channel_name_filter_condition=${channel_name_filter// /\" or channel_name=\"}
	channel_name_state=" (channel_name =\"$channel_name_filter_condition\")"
	arr=("${arr[@]}" "$channel_name_state")
fi
# if channel_name_filter is not null, then aggregate string in format "(channel_name='a' or channel_name='b')", finally put the string into arr

if [ ! -z "$Product_brandOwner" ]
then
	Product_brandOwner_condition=${Product_brandOwner// /\" or b.brandOwner=\"}
	brandOwner_state=" (b.brandOwner =\"$Product_brandOwner_condition\")"
	arr=("${arr[@]}" "$brandOwner_state")
fi
# if channel_name_filter is not null, then aggregate string in format "(brandOwner_state='a' or brandOwner_state='b')", finally put the string into arr

if [ ! -z "$Product_brand" ]
then
	Product_brand_condition=${Product_brand// /\" or b.brand=\"}
	brand_state=" (b.brand =\"$Product_brand_condition\")"
	arr=("${arr[@]}" "$brand_state")
fi
# if channel_name_filter is not null, then aggregate string in format "(brand='a' or brand='b')", finally put the string into arr

if [ ! -z "$Date_end" && ! -z "$Date_start" ]
then
	date_state=" Date>='$Date_start' and Date<='$Date_end'"
	arr=("${arr[@]}" "$date_state")
fi
# if both Date_end and Date_start are not null, then aggregate string in format "Date>='$Date_start' and Date<='$Date_end'", finally put the string into arr

if [ ! -z "$channel_name_filter" || ! -z "$Product_brandOwner" || ! -z "Product_brand" || ! -z "Date_start" || ! -z "Date_end" ]
then
	delim="" ; ARRAY=""
	for item in "${arr[@]}"; do
		ARRAY="$ARRAY$delim$item"
		delim=" and "
	done
	where_statement=" where $ARRAY"
fi
# if both all parameters are not null, then aggrate where statement

OUTPUTTABLE="${OUTPUTTABLE}_${POSTFIX}"

echo "drop table $OUTPUTTABLE"
$MYSQL -e "drop table $OUTPUTTABLE" || true


if [ -z "$els_word" ]
then
	echo "create table $OUTPUTTABLE as (cluster select $columns from sales a inner join Product b on a.Product.id=b.id $where_statement)"
	$MYSQL -e "create table $OUTPUTTABLE as (cluster select $columns from sales a inner join Product b on a.Product.id=b.id $where_statement)"|| true
else 
	echo "create table $OUTPUTTABLE as (cluster select $columns from sales a inner join 
	(select * from Product filter by match(name,'$els_word')) b on a.Product.id=b.id $where_statement)"
	$MYSQL -e "create table $OUTPUTTABLE as (cluster select $columns from sales a inner join 
	(select * from Product filter by match(name,'$els_word')) b on a.Product.id=b.id $where_statement)"|| true
	# if els_word is not null, then use elasticsearch
fi


