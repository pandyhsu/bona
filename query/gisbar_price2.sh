#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}

type_TIMES=\"${TIMES//,/\",\"}\"

CITY=${CITY// /,}
type_CITY=\"${CITY//,/\",\"}\"

AREA=${AREA// /,}
type_AREA=\"${AREA//,/\",\"}\"


if [[ $TIMES != '' ]] ;then
    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) "
    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
    fi
elif [[ $TIMES == '' ]] ;then
    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
        where_state=" and type1 in ($type_CITY) "
    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
        where_state=" and type1 in ($type_CITY) and villages in ($type_AREA) "
    fi
fi

echo $where_state

echo "create table $OUTPUT_TABLE as 
	(remote select villages, count(*) as cnt, '交易數' as filter, '#ffc558' as color from agent.lvr_main_a a
		left join bigobject.citymapping b on a.type1 = b.code
		left join agent.lvr_main_a_detail c on a.id_number = c.id_number
		where not address like '%地號%' and unit_price <> 0 $where_state
		group by villages order by villages asc 
		partition by db='bigobject' and alias = 'TWSBOCLIENT1')"

$MYSQL -e "create table $OUTPUT_TABLE as 
	(remote select villages, count(*) as cnt, '交易數' as filter, '#ffc558' as color from agent.lvr_main_a a
		left join bigobject.citymapping b on a.type1 = b.code
		left join agent.lvr_main_a_detail c on a.id_number = c.id_number
		where not address like '%地號%' and unit_price <> 0 $where_state
		group by villages order by villages asc 
		partition by db='bigobject' and alias = 'TWSBOCLIENT1')"




