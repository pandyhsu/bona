#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="select_times"
echo "drop table $OUTPUT_TABLE"
$MYSQL -e "drop table $OUTPUT_TABLE"|| true

echo "create table $OUTPUT_TABLE as ( remote select time as value, time as name from (select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"
$MYSQL -e "create table $OUTPUT_TABLE as ( remote select time as value, time as name from (select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8) partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"

echo "update $OUTPUT_TABLE set value = '00000' where in (select value from $OUTPUT_TABLE order by value desc limit 1);"
$MYSQL -e "update $OUTPUT_TABLE set value = '00000' where in (select value from $OUTPUT_TABLE order by value desc limit 1);"
