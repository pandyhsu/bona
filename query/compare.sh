#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}

type_TIMES=\"${TIMES//,/\",\"}\"

CITY=${CITY// /,}
type_CITY=\"${CITY//,/\",\"}\"

AREA=${AREA// /,}
type_AREA=\"${AREA//,/\",\"}\"




where_state=""
if [[ $TIMES != '' ]] ;then
	where_state="$where_state and time in ($type_TIMES) "
fi
if [[ $CITY != '' ]] ;then
	where_state="$where_state and type1 in ($type_CITY) "
fi
if [[ $AREA != '' ]] ;then
	where_state="$where_state and villages in ($type_AREA) "
fi

echo $where_state

if [[ $ID != '' ]] ;then

	echo "remote insert into bigobject.userpreference select now() as update_time, '$OPERATOR' as operator, '$ID' as id_number partition by db='bigobject' and alias = 'TWSBOCLIENT1' "
	$MYSQL -e "remote insert into bigobject.userpreference select now() as update_time, '$OPERATOR' as operator, '$ID' as id_number partition by db='bigobject' and alias = 'TWSBOCLIENT1' "
fi


#### DATE、OPERATOR、ID_number
###先create

echo "create table $OUTPUT_TABLE as ( 
	remote select county, town, address, build_tshift_area  ,total_price ,case when trans_sign = '車位' then '--' when trans_sign = '土地' then '--' else unit_price end as unit_price ,case when house_year< 0 then '預售屋' when house_year> 150 then '無資料' else house_year end as house_year ,building_type, car, trans_day ,trans_sign , trans_detail ,room_cnt from 
		(select b.name as county, a.villages as town, address, sum(build_tshift_area * 0.3025) as build_tshift_area, sum(total_price/10000) as total_price, avg((total_price - t_berth_price) / (build_tshift_area - shift_tberth_area) * 3.3058 / 10000) as unit_price,sum(TIMESTAMPDIFF(YEAR,complete_year,trans_day)) as house_year,building_type as building_type  ,case when t_berth_price=0.00 then '無' else '有' end as car, concat(trans_day) as trans_day, trans_sign  as trans_sign ,trans_detail as trans_detail, concat(room ,'房',hall ,'廳',health ,'衛') as room_cnt from agent.lvr_main_a a
			inner join bigobject.userpreference id on a.id_number = id.id_number
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where id.operator='$OPERATOR' and id.update_time >= '$DATE' $where_state
			group by address, id_number)
		partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"

$MYSQL -e "create table $OUTPUT_TABLE as (
	remote select county, town, address, build_tshift_area  ,total_price ,case when trans_sign = '車位' then '--' when trans_sign = '土地' then '--' else unit_price end as unit_price ,case when house_year< 0 then '預售屋' when house_year> 150 then '無資料' else house_year end as house_year ,building_type, car, trans_day ,trans_sign , trans_detail ,room_cnt from 
		(select b.name as county, a.villages as town, address, sum(build_tshift_area * 0.3025) as build_tshift_area, sum(total_price/10000) as total_price, avg((total_price - t_berth_price) / (build_tshift_area - shift_tberth_area) * 3.3058 / 10000) as unit_price,sum(TIMESTAMPDIFF(YEAR,complete_year,trans_day)) as house_year,building_type as building_type  ,case when t_berth_price=0.00 then '無' else '有' end as car, concat(trans_day) as trans_day, trans_sign  as trans_sign ,trans_detail as trans_detail, concat(room ,'房',hall ,'廳',health ,'衛') as room_cnt from agent.lvr_main_a a
			inner join bigobject.userpreference id on a.id_number = id.id_number
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where id.operator='$OPERATOR' and id.update_time >= '$DATE' $where_state
			group by address, id_number)
		partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"






###再insert




##### test 
#echo "====================================test===================================="
#
#
##wktcnt=`$MYSQL -s -N -e " select count(*) as cnt from (apply wkt("$range")) "`
#wktcnt=5
#
#for (( t=0; t<$wktcnt; t++ ))
#do
#	echo $t
#done
#
#		
#echo "====================================end===================================="
#
