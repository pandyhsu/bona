#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}

type_TIMES=\"${TIMES//,/\",\"}\"

CITY=${CITY// /,}
type_CITY=\"${CITY//,/\",\"}\"

AREA=${AREA// /,}
type_AREA=\"${AREA//,/\",\"}\"

#if [[ $TIMES != '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#elif [[ $TIMES == '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" and type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" and type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#fi

#echo $where_state
#以下送進BO做查詢
aa=${id##*--}
aa=\"${aa//,/\",\"}\"

if [[ $id == '' ]] ;then
	#echo "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	if [[ $AREA == '' ]] ;then
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) "
	elif [[ $AREA != '' ]] ;then
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
	fi
elif [[ $id != '' ]] && [[ $AREA == '' ]];then
	#echo "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
	where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($aa)"

elif [[ $id != '' ]] && [[ $AREA != '' ]];then
	aaa=${id##*--}
	AREA2=${AREA}
	if [[ $AREA2 != *"$aaa"* ]] ;then
		#echo "33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333"
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($type_AREA) "
	else
		#echo "55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555"
		where_state=" and time in ($type_TIMES) and type1 in ($type_CITY) and villages in ($aa) "
	fi
fi



echo "create table $OUTPUT_TABLE as (
	( remote select county , villages, address,build_tshift_area, total_price,unit_price, case when house_year< 0 then '0' when house_year> 150 then '無資料' else house_year end as house_year, building_type, car,trans_day, choose, id_number from 
		(select b.name as county , villages, address, sum(build_tshift_area * 0.3025) as build_tshift_area, sum(total_price/10000) as total_price,sum(unit_price *3.3058 /10000) as unit_price, sum(TIMESTAMPDIFF(YEAR,complete_year,trans_day)) as house_year, building_type as building_type, case when t_berth_price=0.00 then '無' else '有' end as car,concat(trans_day) as trans_day,'點選加入' as choose, id_number from agent.lvr_main_a a
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where not address like '%地號%' $where_state
			group by address, id_number)
		partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"

$MYSQL -e "create table $OUTPUT_TABLE as 
	( remote select county , villages, address,build_tshift_area, total_price,unit_price, case when house_year< 0 then '0' when house_year> 150 then '無資料' else house_year end as house_year, building_type, car,trans_day, choose, id_number  from 
		(select b.name as county , villages, address, sum(build_tshift_area * 0.3025) as build_tshift_area, sum(total_price/10000) as total_price,sum(unit_price *3.3058 /10000) as unit_price, sum(TIMESTAMPDIFF(YEAR,complete_year,trans_day)) as house_year, building_type as building_type, case when t_berth_price=0.00 then '無' else '有' end as car,concat(trans_day)  as trans_day,'點選加入' as choose, id_number from agent.lvr_main_a a
			left join bigobject.citymapping b on a.type1 = b.code
			left join agent.lvr_main_a_detail c on a.id_number = c.id_number
			where not address like '%地號%' $where_state
			group by address, id_number)
		partition by db='bigobject' and alias = 'TWSBOCLIENT1' )"

