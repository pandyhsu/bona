#!/bin/bash
set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin

OUTPUTTABLE=Product_brand

BOIP=${BO%%:*}
MYSQL="mysql -h ${BOIP} -P 3306"

echo "Product_brandOwner: $Product_brandOwner"


if [ ! -z "$Product_brandOwner" ]
then
	Product_brandOwner_condition=${Product_brandOwner// /\',\'}
	where_statement=" where b.brandOwner in ('$Product_brandOwner_condition')"
	echo "where_statement: $where_statement"
fi




$MYSQL -e "drop table ${OUTPUTTABLE}_${POSTFIX}"|| true

echo "create table ${OUTPUTTABLE}_${POSTFIX} as (cluster select b.brand as name,b.brand as value from sales a inner join Product b on a.Product.id=b.id $where_statement group by name)"
$MYSQL -e "create table ${OUTPUTTABLE}_${POSTFIX} as (cluster select b.brand as name,b.brand as value from sales a inner join Product b on a.Product.id=b.id $where_statement group by name)"

