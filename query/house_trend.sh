#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin

MYSQL="mysql -h ${BO%:*} -P 3306"
OUTPUT_TABLE="${OUTPUTTABLE}_${POSTFIX}"
$MYSQL -e "drop table $OUTPUT_TABLE"|| true

TIMES2=`$MYSQL -s -N -e " select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 8 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) order by value desc limit 1 "`
TIMES=${TIMES//00000/$TIMES2}
######## 取得前幾季的時間節點
read -ra TIMES_last <<< `$MYSQL -se "select time as value from (remote select time from agent.lvr_main_a where time <> '' group by time order by time desc limit 20 partition by db='bigobject' and alias = 'TWSBOCLIENT1' ) where value < '$TIMES'  order by value desc limit 4"`
TIMES_last5=\'${TIMES_last[0]}\'','\'${TIMES_last[1]}\'','\'${TIMES_last[2]}\'','\'${TIMES_last[3]}\'

type_TIMES=\'${TIMES//,/\',\'}\'
where_time_new=$type_TIMES','$TIMES_last5

CITY=${CITY// /,}
type_CITY=\'${CITY//,/\',\'}\'

AREA=${AREA// /,}
type_AREA=\'${AREA//,/\',\'}\'

#if [[ $TIMES != '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" where time in ($where_time_new) and type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#elif [[ $TIMES == '' ]] ;then
#    if [[ $CITY != '' ]] && [[ $AREA == '' ]] ;then
#        where_state=" where type1 in ($type_CITY) "
#    elif [[ $CITY != '' ]] && [[ $AREA != '' ]] ;then
#        where_state=" where type1 in ($type_CITY) and villages in ($type_AREA) "
#    fi
#fi

#以下送進BO做查詢
aa=${id##*--}
aa=\"${aa//,/\",\"}\"

if [[ $id == '' ]] ;then
	#echo "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	if [[ $AREA == '' ]] ;then
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) "
	elif [[ $AREA != '' ]] ;then
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($type_AREA) "
	fi
elif [[ $id != '' ]] && [[ $AREA == '' ]];then
	#echo "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
	where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($aa)"

elif [[ $id != '' ]] && [[ $AREA != '' ]];then
	aaa=${id##*--}
	AREA2=${AREA}
	if [[ $AREA2 != *"$aaa"* ]] ;then
		#echo "33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333"
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($type_AREA) "
	else
		#echo "55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555"
		where_state=" where time in ($where_time_new) and type1 in ($type_CITY) and villages in ($aa) "
	fi
fi

echo "create table $OUTPUT_TABLE as 
	(select time, villages, round(unit_price * 3.3058/10000,2) as unit_price from 
        (remote select time, villages, avg(unit_price) as unit_price from 
            (select time, unit_price, type1, id_number, address, building_type, villages from agent.lvr_main_a $where_state) a 
            left join bigobject.citymapping b on a.type1 = b.code 
            left join agent.lvr_main_a_detail c on a.id_number = c.id_number 
            where not address like '%地號%' and building_type like '%透天厝%' 
            group by time, villages order by time asc 
            partition by db='bigobject' and alias = 'TWSBOCLIENT1')
        )"

$MYSQL -e "create table $OUTPUT_TABLE as 
	(select time, villages, round(unit_price * 3.3058/10000,2) as unit_price from 
        (remote select time, villages, avg(unit_price) as unit_price from 
            (select time, unit_price, type1, id_number, address, building_type, villages from agent.lvr_main_a $where_state) a 
            left join bigobject.citymapping b on a.type1 = b.code 
            left join agent.lvr_main_a_detail c on a.id_number = c.id_number 
            where not address like '%地號%' and building_type like '%透天厝%' 
            group by time, villages order by time asc 
            partition by db='bigobject' and alias = 'TWSBOCLIENT1')
        )"




if [ -n "$range" ];then
	#echo "====================================test===================================="
	#wktcnt=`$MYSQL -s -N -e " select count(*) as cnt from (apply wkt("$range")) "`
	echo "insert into $OUTPUT_TABLE 
		select time, villages, round(unit_price*1.00/10000,2) as unit_price from 	
			(select time , concat('範圍',b.id) as villages, avg(unit_price) as unit_price from
				(remote select time, join, unit_price, c.ll as ll from 
					(select 1 as join, time, unit_price, id_number from agent.lvr_main_a where time in ($where_time_new) and building_type = '透天厝') a 
					left join (select cast(concat('Point(',longitude,' ',latitude,')') as point) as ll, id_number from agent.lvr_main_a_detail) c on a.id_number = c.id_number 
					partition by db='bigobject' and alias = 'TWSBOCLIENT1')a
				full inner join
				(select id, poly, 1 as join from (apply wkt('$range')) ) b 
				on a.join = b.join where ST_INTERSECTS(a.ll, b.poly)
				group by time, b.id order by unit_price )
			order by time asc "
	$MYSQL -e "insert into $OUTPUT_TABLE 
		select time, villages, round(unit_price*1.00/10000,2) as unit_price from 	
			(select time , concat('範圍',b.id) as villages, avg(unit_price) as unit_price from
				(remote select time, join, unit_price, c.ll as ll from 
					(select 1 as join, time, unit_price, id_number from agent.lvr_main_a where time in ($where_time_new) and building_type = '透天厝') a 
					left join (select cast(concat('Point(',longitude,' ',latitude,')') as point) as ll, id_number from agent.lvr_main_a_detail) c on a.id_number = c.id_number 
					partition by db='bigobject' and alias = 'TWSBOCLIENT1')a
				full inner join
				(select id, poly, 1 as join from (apply wkt('$range')) ) b 
				on a.join = b.join where ST_INTERSECTS(a.ll, b.poly)
				group by time, b.id order by unit_price )
			order by time asc "
	#echo "====================================end===================================="
fi




